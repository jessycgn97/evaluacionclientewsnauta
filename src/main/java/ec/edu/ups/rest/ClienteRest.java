package ec.edu.ups.rest;

import javax.swing.text.html.parser.Entity;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class ClienteRest {
	
	private String WS_PRODUCTO = "http://localhost:8080/EvaluacionWSNauta/rs/carrito/buscarID";
	

	public void producto(int id) {
		Client client = ClientBuilder.newClient();
		
		
		WebTarget target = client.target(
				WS_PRODUCTO).queryParam("id", id);

		target.request().get();
		client.close();	
	}	

}
